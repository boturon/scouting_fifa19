\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Executive Summary}{1}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Customer Overview}{2}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Management Requirements}{3}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Problem Statement}{4}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Data}{10}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}FIFA 19 complete player dataset}{10}{subsection.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Actionable Insight}{11}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Comparison with Lower Ranked Teams}{12}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Comparison with Higher Ranked Teams}{13}{subsection.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Model}{14}{section.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Model Best Team}{14}{subsection.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.1}Data}{14}{subsubsection.7.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.2}Decision Variable}{15}{subsubsection.7.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.3}Constraints}{15}{subsubsection.7.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.4}Objective}{15}{subsubsection.7.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Model Best 11}{16}{subsection.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Suggestions}{17}{section.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1}Optimal Squad; 24 Players}{17}{subsection.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2}Best 11 by Formation (4-4-2, 4-3-3, 3-5-2)}{17}{subsection.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Comparison/justification of changes in players}{19}{section.9}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}New Classification Table}{23}{section.10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {10.1}Comparison between Mar\'{i}timo vs Big Four and Optimal vs Big Four}{23}{subsection.10.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11}Conclusion}{25}{section.11}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}Appendix}{26}{appendix.A}%
